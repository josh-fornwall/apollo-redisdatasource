# redisDataSource

A Redis Data Source for Apollo Server GraphQL.

## Getting started

Install with yarn

     yarn add apollo-redisdatasource --registry=https://gitlab.com/api/v4/projects/32783882/packages/npm

Import into your project. Initialize.
