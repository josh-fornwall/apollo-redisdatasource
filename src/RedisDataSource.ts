import { DataSource } from 'apollo-datasource';
import { RedisIndex } from './RedisIndex';
import { RedisKeyIndex } from "./RedisKeyIndex";
import * as Redis from 'ioredis';

import * as crypto from 'crypto';

export class RedisDataSource<T> extends DataSource {
  private _redis;
  private _indices: RedisIndex<T>[];

  constructor(uri: string, 
              private _keyField: string, 
              private _prefix: string) {
    super();
    this._redis = new Redis(uri, { lazyConnect: false });
    this._prefix = _prefix.replace(/:+$/, '');
    this._indices = new Array<RedisIndex<T>>();
  }

  get Redis() { return this._redis; }

  private genId(): string {
    return crypto.randomUUID();
  }

  async addIndex(index: RedisIndex<T>) {
      this._indices.push(index);
  }

  async getObject(id: string): Promise<T> {
    if (!id.startsWith(this._prefix)) {
      id = `${this._prefix}:${id}`;
    }

    const item = await this._redis.hgetall(id);
    return item as unknown as T;
  }

  async *getAllObjects(index?: string): AsyncGenerator<T> {
    const keys = await this._redis.keys(`${this._prefix}:*`);
    for (const key of keys) {
      yield await this.getObject(key);
    }
  }

  async deleteObject(id: string) {
    const obj = await this.getObject(id);
    if( obj == null ) {
        return;
    }

    await this._redis.del(`${this._prefix}:${id}`);
    for( const idx of this._indices ) {
        await idx.removeItem(obj);
    }
  }

  async postObject(obj: T): Promise<T | boolean> {
    for( const i of this._indices ) {
      if( i instanceof RedisKeyIndex ) {
        const itemId = await i.find(obj);
        if( itemId != null ) {
            return false;
        }
      }
    }

    // Doesn't exist, so create.
    const newId = this.genId();

    const oa = obj as any;
    oa[this._keyField] = newId;
    await this._redis.hset(`${this._prefix}:${newId}`, oa);
    for( const idx of this._indices ) {
        await idx.addItem(obj, this._keyField);
    }
    return oa as T;
  }

  async putObject(obj: T) {
    const oa = obj as any;
    const myId = oa[this._keyField];
    const hashKey = `${this._prefix}:${myId}`;
    const exists = await this._redis.exists(hashKey);
    if( exists ) {
      await this._redis.hset(hashKey, oa);
      // Might need to do something with indicies?

      return this.getObject(myId);
    } else {
      return null;
    }
  }
}
