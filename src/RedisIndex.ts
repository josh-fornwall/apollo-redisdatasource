
export abstract class RedisIndex<T> {
  abstract addItem(item: T, value: string): Promise<void>;
  abstract removeItem(item: T): Promise<void>;
}
