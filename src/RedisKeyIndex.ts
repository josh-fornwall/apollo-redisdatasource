import * as Redis from 'ioredis';
import { RedisIndex } from './RedisIndex';



export class RedisKeyIndex<T> implements RedisIndex<T> {
  private _redis: Redis.Redis;

  constructor(
    private _fields: [keyof T],
    redis: Redis.Redis | string,
    private _name: string = "") {
    if (typeof redis === "string") {
      this._redis = new Redis(redis, { lazyConnect: false });
    } else {
      this._redis = redis;
    }

    this.Name = "urn:idx-key:" + (_name.length > 0 ? _name : _fields.join(":"));
  }

  public Name: string;

  public async addItem(item: T, keyField: string) {
    await this._redis.hset(this.Name, this.keyGenerator(item), (item as any)[keyField].toString());
  }

  public async find(item: T) {
    return await this._redis.hget(this.Name, this.keyGenerator(item));
  }

  public async removeItem(item: T) {
    await this._redis.hdel(this.Name, this.keyGenerator(item));
  }

  private keyGenerator(item: T): string {
    const obj = item as any;
    return this._fields.map(f => obj[f]).join(':');
  }
}
