import * as Redis from 'ioredis';
import { RedisDataSource } from './RedisDataSource';
import { RedisIndex } from './RedisIndex';


export class RedisListIndex<T> implements RedisIndex<T> {
  private _redis: Redis.Redis;

  constructor(
    private _keyField: string,
    redis: Redis.Redis | string,
    private _name: string = "",
    x: new () => T) {
    if (typeof redis === "string") {
      this._redis = new Redis(redis, { lazyConnect: false });
    } else {
      this._redis = redis;
    }

    if( typeof x === "undefined" && _name.length === 0 ) { _name = "undefined" }
    this.Name = "urn:idx-list:" + (_name.length > 0 ? _name : `${x.name}:${_keyField}`);
  }

  public Name: string;

  public async addItem(item: T) {
    await this._redis.sadd(this.Name, (item as any)[this._keyField].toString());
  }

  public async exists(item: T): Promise<boolean> {
    const br = await this._redis.sismember(this.Name, (item as any)[this._keyField].toString());
    return Boolean(br);
  }

  public async removeItem(item: T) {
    await this._redis.srem(this.Name, (item as any)[this._keyField].toString());
  }

  public async *items(ds: RedisDataSource<T>) {
    const ids = await this._redis.smembers(this.Name);
    for (const key of ids) {
      yield await ds.getObject(key);
    }
  }
}
