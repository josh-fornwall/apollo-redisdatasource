import * as Redis from 'ioredis';
import { RedisDataSource } from './RedisDataSource';
import { RedisIndex } from './RedisIndex';



export class RedisOneToManyIndex<T> implements RedisIndex<T> {
  private _redis: Redis.Redis;

  constructor(
    private _manyField: string,
    private _keyField: string,
    redis: Redis.Redis | string,
    private _name: string = "") {
    if (typeof redis === "string") {
      this._redis = new Redis(redis, { lazyConnect: false });
    } else {
      this._redis = redis;
    }

    this.Name = "urn:idx-o2m:" + (_name.length > 0 ? _name : `${_keyField}:${_manyField}`);
  }

  public Name: string;

  public async addItem(item: T, value: string): Promise<void> {
    const aitem = item as any;
    this._redis.sadd(`${this.Name}:${aitem[this._keyField]}`, aitem[this._manyField]);
  }

  public async removeItem(item: T): Promise<void> {
    const aitem = item as any;
    this._redis.srem(`${this.Name}:${aitem[this._keyField]}`, aitem[this._manyField]);
  }

  public async *items(key: string, ds: RedisDataSource<T>) {
    const ids = await this._redis.smembers(`${this.Name}:${key}`);
    for (const mem of ids) {
      yield await ds.getObject(mem);
    }
  }
}
