import * as Redis from 'ioredis';
import { RedisIndex } from './RedisIndex';


export class RedisRangeIndex<T> implements RedisIndex<T> {
  private _redis: Redis.Redis;

  constructor(
    private _keyField: string,
    private _valueField: string,
    redis: Redis.Redis | string,
    private _name: string = "",
    x: new () => T) {
    if (typeof redis === "string") {
      this._redis = new Redis(redis, { lazyConnect: false });
    } else {
      this._redis = redis;
    }

    if( typeof x === "undefined" && _name.length === 0 ) { _name = "undefined" }
    this.Name = "urn:idx-range:" + (_name.length > 0 ? _name : `${x.name}:${_keyField}:${_valueField}`);
  }

  public Name: string;

  public async addItem(item: T) {
    const aitem = item as any;
    await this._redis.zadd(this.Name, aitem[this._valueField], aitem[this._keyField]);
  }

  public async removeItem(item: T) {
    await this._redis.zrem(this.Name, (item as any)[this._keyField]);
  }

  public async findInRange(start: number | string, end: number | string) {
    await this._redis.zrangebyscore(this._name, start, end);
  }
}
